package page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import utility.Utility;

public class PageFalabella extends Utility{
	
	By textBuscar = By.cssSelector("nav.Header-module_navHeader__298G-:nth-child(2) div.d-none.d-lg-block div.container.d-flex.flex-row-reverse div.flex-grow-1 div.search-bar.SearchBar-module_search-bar__2DhTP div.d-none.d-lg-flex.w-100:nth-child(1) div.DesktopSearchBar-module_search-bar__1PiDn.desktop-search-bar > input.DesktopSearchBar-module_searchbox-input__HXYgR");
	By precios = By.cssSelector("span.jsx-185326735.copy1.normal");
	By productos = By.cssSelector("a.pod-summary.pod-link");
	By color = By.cssSelector("ul.jsx-1939264011 li:last-child button");
	By tallas = By.cssSelector("div.size-options div button");
	By bolsa = By.cssSelector("button.button i.csicon-arrow_right");
	By carro = By.id("linkButton");
	By compra = By.cssSelector("button.js-fb-continue-purchase");
	By region = By.id("region");
	By comuna = By.id("comuna");
	By envio = By.cssSelector("button.fbra_formItem__field04");
	By calle = By.id("calle");
	By numeroCalle = By.id("streetNumber");
	By numeroDepartamento = By.id("departmentNumber");
	By btnDireccion = By.cssSelector("button.fbra_formItem__useAddress.fbra_test_button");
	By btnCheckout = By.cssSelector("button.fbra_test_checkoutComponentDeliveryActions__continueToSecurePaymentButton");
	
	public PageFalabella(WebDriver driver) {
		super();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}	
	
	public Boolean buscar(String Busqueda) {
		try {
			type(Busqueda, textBuscar);
			findElement(textBuscar).sendKeys(Keys.ENTER);
			return true;
		}catch (Throwable ea){
			System.out.println(ea.getMessage());
			return false;
		}		
	}
	
	public Boolean comparar () {
		try {
			Thread.sleep(2000);
			List<WebElement> arregloPrecios = findElements(precios);
			List<WebElement> arregloProductos = findElements(productos);
			int tama�o = arregloPrecios.size() - 1;
			int[] arreglo =  new int [tama�o];			
			for (int i = 0; i < tama�o; i++) {
				String d = arregloPrecios.get(i).getText();
				String limpia  = d.toString().replace(" ", "").replace("$", "").replace("(Oferta)", "").replace(".", "").replace("-", "");
				arreglo[i]= Integer.parseInt(limpia);							
			}
			
			int a = masCercano(arreglo);
			arregloProductos.get(a).click();
			Thread.sleep(2000);
			return true;
			
		}catch (Throwable eb){
			System.out.println(eb.getMessage());
			return false;
		}		
	}
	
	public static int masCercano(int[] numeros) {
        int num = 10000;
		int indice = 0;
        int diferencia = Integer.MAX_VALUE;
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == num) {
                return i;
            } else {
                if(Math.abs(numeros[i]-num)<diferencia){
                    indice=i;
                    diferencia = Math.abs(numeros[i]-num);
                }
            }
        }
        return indice;
    }
	
	public Boolean color() {
		try {
			click(color);
			Thread.sleep(2000);
			return true;
		}catch (Throwable ec){
			System.out.println(ec.getMessage());
			return false;
		}		
	}
	
	public Boolean size() {
		try {
			List<WebElement> Tallas = findElements(tallas);
			int tama�o =Tallas.size() - 1;
			for (int i = 0; i < tama�o; i++) {
	            if (Tallas.get(i).isEnabled()) {
	                Tallas.get(i).click();
	                Thread.sleep(2000);
	            	return true;
	            }
	        }
			System.out.println("No hay opciones de tama�o");
	        Thread.sleep(2000);
			return true;						
		}catch (Throwable ec){
			System.out.println(ec.getMessage());
			return false;
		}		
	}
	
	public Boolean bolsa() {
		try {
			click(bolsa);
			Thread.sleep(2000);
			return true;
		}catch (Throwable ed){
			System.out.println(ed.getMessage());
			return false;
		}		
	}
	
	public Boolean carro() {
		try {
			click(carro);
			Thread.sleep(2000);
			return true;
		}catch (Throwable ee){
			System.out.println(ee.getMessage());
			return false;
		}		
	}
	
	public Boolean compra() {
		try {
			click(compra);
			Thread.sleep(2000);
			return true;
		}catch (Throwable ef){
			System.out.println(ef.getMessage());
			return false;
		}		
	}
	
	public Boolean envio() {
		try {
			selectDropdown(region,"REGION METROPOLITANA");
			Thread.sleep(2000);
			selectDropdown(comuna,"SANTIAGO CENTRO");
			Thread.sleep(2000);
			click(envio);
			Thread.sleep(2000);
			type("Santa Isaabel", calle);
			type("512", numeroCalle);
			type("1910", numeroDepartamento);
			click(btnDireccion);
			Thread.sleep(2000);
			click(btnCheckout);
			
			return true;
		}catch (Throwable eg){
			System.out.println(eg.getMessage());
			return false;
		}		
	}

}
