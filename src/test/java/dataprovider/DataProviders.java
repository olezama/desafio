package dataprovider;

import java.io.FileNotFoundException;

import org.testng.annotations.DataProvider;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;


public class DataProviders {
	
	@DataProvider (name ="BusquedaData")
	  public Object[][] passData1() throws JsonIOException, JsonSyntaxException, FileNotFoundException{
		  
		  String Path = "C:\\Users\\oswal\\eclipse-workspace\\desafio4\\src\\test\\resources\\Json\\BusquedaData.json";
		
		  return JsonReader.getdata(Path, "Publication Data", 1, 1);
		  
	  }

}
