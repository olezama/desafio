package test;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import base.Base;
import dataprovider.DataProviders;
import page.PageFalabella;

public class TestFalabella extends Base{
	
	  @Test(description="Busqueda de producto", dataProvider ="BusquedaData",  dataProviderClass=DataProviders.class)
	  public void Publish(String Busqueda) {
		  
		  PageFalabella pageFalabella = new PageFalabella(driver);
		  SoftAssert softAssertion= new SoftAssert();
		  
		  softAssertion.assertTrue(pageFalabella.buscar(Busqueda), "No se puede realizar Busqueda");
		  softAssertion.assertTrue(pageFalabella.comparar(), "No se puede comparar precios");
		  softAssertion.assertTrue(pageFalabella.color(), "No se puede Seleccionar color");
		  softAssertion.assertTrue(pageFalabella.size(), "No se puede Seleccionar el tama�o");
		  softAssertion.assertTrue(pageFalabella.bolsa(), "No se puede Seleccionar la bolsa");
		  softAssertion.assertTrue(pageFalabella.carro(), "No se puede ir al carro");
		  softAssertion.assertTrue(pageFalabella.compra(), "No se puede ir a confirmar carro");
		  softAssertion.assertTrue(pageFalabella.envio(), "No se puede Seleccionar la direccion de envio");
		  softAssertion.assertAll();
	  		
	  }

}
